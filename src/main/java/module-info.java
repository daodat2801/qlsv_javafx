module QLSV.FX {
    requires javafx.base;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires lombok;
    requires unirest.java;
    requires com.google.gson;

    exports sample;
    opens sample.controller to javafx.fxml;
    opens sample.model to javafx.base;
    opens view;
}