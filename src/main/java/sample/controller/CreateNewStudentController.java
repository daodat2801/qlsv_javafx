package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.model.Student;
import sample.service.StudentService;
import sample.service.StudentServiceImpl;

import java.io.IOException;
import java.util.Optional;

public class CreateNewStudentController {
    @FXML
    TextField nameTxt;
    @FXML
    TextField ageTxt;
    @FXML
    TextField addressTxt;
    final StudentService service = new StudentServiceImpl();

    public void save(ActionEvent event) throws Exception {
        Student student = new Student();
        student.setName(nameTxt.getText());
        student.setAge(Integer.parseInt(ageTxt.getText()));
        student.setAddress(addressTxt.getText());
        Boolean result=service.sendNewStudentInfo(student);
        if(result){
            Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Thêm sinh viên thành công!");
            ButtonType ok=new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
            alert.getButtonTypes().setAll(ok);
            Optional<ButtonType> buttonEvent= alert.showAndWait();
            if(buttonEvent.get()==ok){
                back(event);
            }
        }
        else {
            Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Thêm sinh viên thất bại!");
            alert.show();
        }
    }

    public void back(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/MainView.fxml"));
        Parent parent = loader.load();
        MainController controller = loader.getController();
        stage.setScene(new Scene(parent));
        stage.show();
    }
}
