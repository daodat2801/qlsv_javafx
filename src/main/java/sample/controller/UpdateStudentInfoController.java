package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.Setter;
import sample.model.Student;
import sample.service.StudentService;
import sample.service.StudentServiceImpl;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class UpdateStudentInfoController {
    @FXML
    TextField nameTxt;
    @FXML
    TextField ageTxt;
    @FXML
    TextField addressTxt;
    StudentService service=new StudentServiceImpl();
    Student student;
    public void setStudent(Student student){
        this.student=student;
        nameTxt.setText(student.getName());
        ageTxt.setText(String.valueOf(student.getAge()));
        addressTxt.setText(student.getAddress());
    }
    public void save(ActionEvent event) throws Exception {
        student.setName(nameTxt.getText());
        student.setAge(Integer.parseInt(ageTxt.getText()));
        student.setAddress(addressTxt.getText());
        Boolean result=service.sendUpdatedStudentInfo(student);
        if(result){
            Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Sửa sinh viên thành công!");
            ButtonType ok=new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
            alert.getButtonTypes().setAll(ok);
            Optional<ButtonType> buttonEvent= alert.showAndWait();
            if(buttonEvent.get()==ok){
                back(event);
            }
        }
        else {
            Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Thêm sinh viên thất bại!");
            alert.show();
        }
    }
    public void back(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/MainView.fxml"));
        Parent parent = loader.load();
        MainController controller = loader.getController();
        stage.setScene(new Scene(parent));
        stage.show();
    }
}
