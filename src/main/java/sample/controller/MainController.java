package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainController {
    public void goToStudentListView(ActionEvent event) throws Exception{
        Stage stage=(Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader= new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/StudentListView.fxml"));
        Parent parent=loader.load();
        StudentListController controller=loader.getController();
        stage.setScene(new Scene(parent));
    }
    public void goToCreateNewStudentView(ActionEvent event) throws Exception{
        Stage stage=(Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader= new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/CreateNewStudentView.fxml"));
        Parent parent=loader.load();
        CreateNewStudentController controller=loader.getController();
        stage.setScene(new Scene(parent));

    }
    public void goToUpdateStudentInforView(ActionEvent event) throws Exception{
        Stage stage=(Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader= new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/SearchStudentByNameView.fxml"));
        Parent parent=loader.load();
        SearchStudentByNameController controller=loader.getController();
        controller.setNextScene(true);
        stage.setScene(new Scene(parent));

    }
    public void goToDeleteStudentView(ActionEvent event) throws Exception{
        Stage stage=(Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader= new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/SearchStudentByNameView.fxml"));
        Parent parent=loader.load();
        SearchStudentByNameController controller=loader.getController();
        stage.setScene(new Scene(parent));

    }
}
