package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lombok.Setter;
import sample.model.Student;
import sample.service.StudentService;
import sample.service.StudentServiceImpl;

import java.io.IOException;
import java.util.Optional;

public class SearchStudentByNameController {
    @FXML
    TextField nameTxt;
    @FXML
    Button okBtn;
    @FXML
    ListView<Student> listView;
    final StudentService service = new StudentServiceImpl();
    ObservableList<Student> students;
    @Setter
    Boolean nextScene=false;


    public void search(ActionEvent event) {
        students = FXCollections.observableList(service.getStudentsByName(nameTxt.getText()));
        listView.setItems(students);
        listView.setOnMouseClicked(mouseEvent -> {
            okBtn.setDisable(false);
        });
    }

    public void ok(ActionEvent event) throws Exception {
        Student student = listView.getSelectionModel().getSelectedItem();
        if (nextScene) {
            handleUpdate(event, student);
        } else {
            handleDelete(event, student);
        }
    }

    public void handleUpdate(ActionEvent event, Student student) throws Exception {

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/UpdateStudentInfoView.fxml"));
        Parent parent = loader.load();
        UpdateStudentInfoController controller = loader.getController();
        controller.setStudent(student);
        stage.setScene(new Scene(parent));
        stage.show();
    }

    public void handleDelete(ActionEvent event,Student student) throws IOException {
        Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Xóa "+student.getName()+" khỏi danh sách sinh viên?");
        ButtonType ok=new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel=new ButtonType("CANCEL", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(ok,cancel);
        Optional<ButtonType> optional= alert.showAndWait();
        if(optional.get()==ok){
            Boolean result = service.sendDeletedStudentInfo(student.getStudentId());
            if(result){
                alert.setContentText("Xóa sinh viên thành công!");
                alert.show();
            }else {
                alert.setContentText("Xóa sinh viên thất bại!");
                alert.show();
            }
            back(event);
        }
    }

    public void back(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/MainView.fxml"));
        Parent parent = loader.load();
        MainController controller = loader.getController();
        stage.setScene(new Scene(parent));
        stage.show();
    }
}
