package sample.service;

import sample.model.Student;
import java.util.List;

public interface StudentService {
    List<Student> getAllStudent();
    List<Student> getStudentsByName(String name);
    boolean sendNewStudentInfo(Student student);
    boolean sendUpdatedStudentInfo(Student student);
    boolean sendDeletedStudentInfo(int studentId);

}
