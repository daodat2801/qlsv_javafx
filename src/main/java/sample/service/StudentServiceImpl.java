package sample.service;

import kong.unirest.*;
import sample.model.Student;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    private Object Student;

    @Override
    public List<Student> getAllStudent() {
        List<Student> response = Unirest.get("http://localhost:8080/studentMGMT/getAll")
                .asObject(new GenericType<List<Student>>() {
                }).getBody();
        return response;
    }

    @Override
    public List<Student> getStudentsByName(String name) {
        List<Student> response = Unirest.get("http://localhost:8080/studentMGMT/get/" + name)
                .asObject(new GenericType<List<Student>>() {
                }).getBody();
        return response;
    }

    @Override
    public boolean sendNewStudentInfo(Student student) {
        Student response = Unirest.post("http://localhost:8080/studentMGMT/create")
                .header("Content-Type", "application/json")
                .body(student)
                .asObject(new GenericType<Student>() {
                }).getBody();
        if (response.getName().equals(student.getName())) {
            return true;
        } else
            return false;
    }

    @Override
    public boolean sendUpdatedStudentInfo(Student student) {
        Student response = Unirest.put("http://localhost:8080/studentMGMT/update")
                .header("Content-Type", "application/json")
                .body(student)
                .asObject(new GenericType<Student>() {
                }).getBody();
        if (response.getName().equals(student.getName())) {
            return true;
        } else
            return false;
    }

    @Override
    public boolean sendDeletedStudentInfo(int studentId) {
        String response = Unirest.delete("http://localhost:8080/studentMGMT/delete/"+studentId)
                .asObject(new GenericType<String>() {
                }).getBody();
        if(response.equals("ok"))
            return true;
        else
            return false;
    }
}
