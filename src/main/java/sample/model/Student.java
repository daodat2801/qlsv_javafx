package sample.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private int studentId;
    private String name;
    private int age;
    private String address;

    @Override
    public String toString(){
        return "Mã sinh viên: "+studentId+" Tên sinh viên: "+name;
    }
}
